import React, { Component } from 'react';
import ItemShoe from './ItemShoe';

export default class ListShoe extends Component {
  renderShoeItem = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemShoe
          item={item}
          key={index}
          handleOpenModalDetailProduct={this.props.handleOpenModalDetailProduct}
          handleAddToCart={this.props.handleAddToCart}
        />
      );
    });
  };

  render() {
    return (
      <div className="list-shoe container">
        <div className="row g-3">{this.renderShoeItem()}</div>
      </div>
    );
  }
}
