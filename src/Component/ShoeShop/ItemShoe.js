import React, { Component } from 'react';

export default class ItemShoe extends Component {
  render() {
    let { name, image, price } = this.props.item;

    return (
      <div className="col-3">
        <div className="card">
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text fs-4">Giá: {price}$</p>
            <div className="d-flex justify-content-between">
              <button
                className="btn btn-warning"
                onClick={() => {
                  this.props.handleOpenModalDetailProduct(this.props.item);
                }}
              >
                Xem chi tiết
              </button>
              <button
                className="btn btn-primary"
                onClick={() => {
                  this.props.handleAddToCart(this.props.item);
                }}
              >
                Mua hàng
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
