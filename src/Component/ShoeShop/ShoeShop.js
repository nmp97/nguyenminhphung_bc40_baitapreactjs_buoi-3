import React, { Component } from 'react';
import ListShoe from './ListShoe';
import { dataShoe } from '../../data/dataShoe';
import Header from '../Header/Header';
import ModalDetailProduct from '../Modal/ModalDetailProduct/ModalDetailProduct';
import ModalCart from '../Modal/ModalCart/ModalCart';
import Footer from '../Footer/Footer';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default class ShoeShop extends Component {
  state = {
    listShoe: dataShoe,
    showModalDetailProduct: false,
    showModalCart: false,
    detailProduct: {},
    cart: [],
  };

  handleNotifySuccess = () =>
    toast('Đã thêm vào giỏ hàng.', { type: 'success', position: 'top-center', autoClose: 1000 });

  handleNotifyRemove = () =>
    toast('Đã xóa thành công.', { type: 'error', position: 'top-right', autoClose: 1000 });

  handleOpenModalDetailProduct = (item) => {
    this.setState({ showModalDetailProduct: true });
    this.setState({ detailProduct: item });
  };

  handleCloseModalDetailProduct = () => {
    this.setState({ showModalDetailProduct: false });
  };

  handleCloseModalCart = () => {
    this.setState({ showModalCart: false });
  };

  handleOpenModalCart = () => {
    this.setState({ showModalCart: true });
  };

  handleAddToCart = (item) => {
    let newCart = [...this.state.cart];

    let index = newCart.findIndex((cart) => {
      return cart.id === item.id;
    });

    if (index === -1) {
      let newItem = { ...item, quantity: 1 };

      newCart.push(newItem);
    } else {
      newCart[index].quantity += 1;
    }

    this.setState({ cart: newCart });

    this.handleNotifySuccess();
  };

  handleIncreaseQuantityModalCart = (item) => {
    let newCart = [...this.state.cart];

    let index = newCart.findIndex((cart) => {
      return cart.id === item.id;
    });

    newCart[index].quantity++;

    this.setState({ cart: newCart });
  };

  handleDecreaseQuantityModalCart = (item) => {
    let newCart = [...this.state.cart];

    let index = newCart.findIndex((cart) => {
      return cart.id === item.id;
    });

    let quantityItem = newCart[index].quantity;

    if (quantityItem >= 2) {
      newCart[index].quantity--;
    } else {
      newCart.splice(index, 1);
    }

    this.setState({ cart: newCart });
  };

  handleRemoveCartItem = (cart) => {
    let newCart = [...this.state.cart];
    let index = newCart.findIndex((item) => {
      return cart.id === item.id;
    });

    newCart.splice(index, 1);

    this.setState({ cart: newCart });
    this.handleNotifyRemove();
  };

  handleTotalModalCart = () => {
    let newCart = [...this.state.cart];
    let total = 0;

    for (let i = 0; i < newCart.length; i++) {
      let { price, quantity } = newCart[i];
      total += price * quantity;
    }
    return total;
  };

  handleQuantityCart = () => {
    let newCart = [...this.state.cart];
    let quantity = 0;

    if (newCart.length === 0) return (quantity = 0);

    for (let i = 0; i < newCart.length; i++) {
      let quantityItem = newCart[i].quantity * 1;
      quantity += quantityItem;
    }
    return quantity;
  };

  render() {
    return (
      <>
        <div className="content">
          <Header
            handleOpenModalCart={this.handleOpenModalCart}
            handleQuantityCart={this.handleQuantityCart}
          />

          <ModalCart
            cartList={this.state.cart}
            handleOpenModalCart={this.state.showModalCart}
            handleCloseModalCart={this.handleCloseModalCart}
            handleIncreaseQuantityModalCart={this.handleIncreaseQuantityModalCart}
            handleDecreaseQuantityModalCart={this.handleDecreaseQuantityModalCart}
            handleRemoveCartItem={this.handleRemoveCartItem}
            handleTotalModalCart={this.handleTotalModalCart}
          />

          <ModalDetailProduct
            showModalDetailProduct={this.state.showModalDetailProduct}
            item={this.state.detailProduct}
            handleCloseModalDetailProduct={this.handleCloseModalDetailProduct}
          />

          <ListShoe
            list={this.state.listShoe}
            handleOpenModalDetailProduct={this.handleOpenModalDetailProduct}
            handleAddToCart={this.handleAddToCart}
          />

          <ToastContainer />

          <Footer />
        </div>
      </>
    );
  }
}
