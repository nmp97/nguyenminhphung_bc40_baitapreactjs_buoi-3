import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

export default class ModalDetailProduct extends Component {
  render() {
    let { name, price, description, image } = this.props.item;
    return (
      <Modal
        show={this.props.showModalDetailProduct}
        onHide={this.props.handleCloseModalDetailProduct}
      >
        <Modal.Header closeButton>
          <Modal.Title className="fs-2 fw-bold">Thông tin sản phẩm</Modal.Title>
        </Modal.Header>
        <Modal.Body className="d-flex">
          <img src={image} alt="" style={{ width: '15rem' }} />
          <div className="p-3">
            <h4>{name}</h4>
            <p className="fw-bold">Giá: {price}$</p>
            <p>
              <strong>Mô tả:</strong>
              <br />
              {description}
            </p>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={this.props.handleCloseModalDetailProduct}>
            Đóng
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}
