import React, { Component } from 'react';
import Modal from 'react-bootstrap/Modal';
import CartItem from './CartItem';
import EmptyCart from './EmptyCart';
import FooterCart from './FooterCart';

export default class ModalCart extends Component {
  render() {
    return (
      <div>
        <Modal show={this.props.handleOpenModalCart} onHide={this.props.handleCloseModalCart}>
          <Modal.Header closeButton>
            <Modal.Title>Giỏ Hàng Của Bạn</Modal.Title>
          </Modal.Header>
          {this.props.cartList.length === 0 ? (
            <EmptyCart handleCloseModalCart={this.props.handleCloseModalCart} />
          ) : (
            <>
              <Modal.Body>
                <CartItem
                  cartList={this.props.cartList}
                  handleIncreaseQuantityModalCart={this.props.handleIncreaseQuantityModalCart}
                  handleDecreaseQuantityModalCart={this.props.handleDecreaseQuantityModalCart}
                  handleRemoveCartItem={this.props.handleRemoveCartItem}
                />
              </Modal.Body>
              <Modal.Footer className="d-block">
                <FooterCart
                  handleCloseModalCart={this.props.handleCloseModalCart}
                  handleTotalModalCart={this.props.handleTotalModalCart}
                />
              </Modal.Footer>
            </>
          )}
        </Modal>
      </div>
    );
  }
}
