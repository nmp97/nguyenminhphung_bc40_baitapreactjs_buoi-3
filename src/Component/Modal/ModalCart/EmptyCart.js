import React, { Component } from 'react';

export default class EmptyCart extends Component {
  render() {
    return (
      <div className="text-center py-5">
        <p>Chưa có sản phẩm</p>
        <button className="btn btn-outline-primary" onClick={this.props.handleCloseModalCart}>
          Tiếp tục mua sắm
        </button>
      </div>
    );
  }
}
