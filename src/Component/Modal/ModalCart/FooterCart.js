import React, { Component } from 'react';

export default class FooterCart extends Component {
  render() {
    return (
      <>
        <div className="d-flex justify-content-between align-content-center">
          <span>Tổng Tiền:</span>
          <span>{this.props.handleTotalModalCart()}$</span>
        </div>
        <div className="text-end mt-3">
          <button
            className="mx-2 btn btn-danger"
            variant="secondary"
            onClick={this.props.handleCloseModalCart}
          >
            Đóng
          </button>
          <button
            className="btn btn-primary"
            variant="primary"
            onClick={this.props.handleCloseModalCart}
          >
            Đặt hàng
          </button>
        </div>
      </>
    );
  }
}
